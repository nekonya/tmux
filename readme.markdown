# my tmux setting

## clone my remote repository

```sh
$ git clone https://gitlab.com/nekonya/tmux.git .tmux
```

## put .tmux.conf symbolic link to home directory

```sh
# ln -fs ${HOME}/.tmux/.tmux.conf ${HOME}
```

## install [Tmux Plugin Manager](https://github.com/tmux-plugins/tpm)

```sh
$ git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
```

## install [tmux-mem-cpu-load](https://github.com/thewtex/tmux-mem-cpu-load)

```sh
$ git clone https://github.com/thewtex/tmux-mem-cpu-load.git ~/.tmux/plugins/tmux-mem-cpu-load
$ cd ~/.tmux/plugins/tmux-mem-cpu-load
$ cmake .
$ make
```

